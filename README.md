# UX and Outreach

[![License: MIT-0](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/license/mit-0) [![CHAOSS DEI: Bronze](https://gitlab.com/tgdp/governance/-/raw/main/CHAOSS-Bronze-Badge-small.png?ref_type=heads)](https://badging.chaoss.community/)

The UX and Outreach repository is a space for collaborating with the UX and Outreach working group for [The Good Docs Project](https://www.thegooddocsproject.dev/).

The UX and Outreach working group works on mission critical initiatives to support the template user experience and to help us grow our user community. This group conducts user research, helps set the template roadmap, and works on growing our core user base through various communication channels.

Join this group if you would like to develop complementary skills in UX, product management, copywriting, technical marketing, and more.
