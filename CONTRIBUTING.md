# The Good Docs Project - Contributing guide

Welcome to the Good Docs Project! If you’re reading this guide, that probably means you'd like to get involved and start contributing to the project.

To read our project's general CONTRIBUTING guide, see [CONTRIBUTING](https://gitlab.com/tgdp/governance/-/blob/main/CONTRIBUTING.md).