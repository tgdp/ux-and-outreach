# Support

To read our SUPPORT policy, see [SUPPORT.md](https://gitlab.com/tgdp/governance/-/blob/main/SUPPORT.md?ref_type=heads) in the Governance and Community repository.